<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <title>Album Details</title>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="admin/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="admin/jquery/jquery.min.js"></script>
  <script src="admin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-3"><h2>Album Details</h2></div>
        <div class="col-md-3"></div>
      <div class="col-md-3"></div>
    </div>

<?php
  include("admin/confs/config.php");
  $id = $_GET['id'];
  $albums = mysql_query("SELECT * FROM albums WHERE id = $id");
  $row = mysql_fetch_assoc($albums);
?>

  <img src="admin/covers/<?php echo $row['cover'] ?>">
  <h3><?php echo $row['title'] ?></h3>
  <b>Singer:</b><i><?php echo $row['vocalist'] ?></i><br>
  <b>Price:</b>$<i><?php echo $row['price'] ?></i>
  <p><b>Summary:</b><i><?php echo $row['summary'] ?></i></p>

  <a class="btn btn-primary" href="add-to-cart.php?id=<?php echo $id ?>">
    Add to Cart
  </a>
  <a href="index.php" class="btn btn-info">&laquo; Go Back</a> 
</div>
</body>
</html>