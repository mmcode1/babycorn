<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>Album List</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
  	<script src="jquery/jquery.min.js"></script>
 	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
</head>
<body>
	<div class="container">
	<div class="row">
  	<div class="col-md-6">
		<h2>Album List</h2>
		<br>
		<a class="btn btn-primary" href="album-list.php">Manage Albums</a>
		<a class="btn btn-primary" href="cat-list.php">Manage Categories</a>
		<a class="btn btn-primary" href="orders.php">Manage Orders</a>
		<a class="btn btn-danger" href="logout.php">Logout</a>
		<div>&nbsp;</div>
		<?php
		include("confs/auth.php");
		include("confs/config.php");
		$result = mysql_query("
		SELECT albums.*, categories.name
		FROM albums LEFT JOIN categories
		ON albums.category_id = categories.id
		ORDER BY albums.created_date DESC
		");
		?>
		<?php while($row = mysql_fetch_assoc($result)): ?>
			<img class="text-center" src="covers/<?php echo $row['cover'] ?>"	
			class="img-rounded">
		<br>
		<b>Title: <?php echo $row['title'] ?></b><br>
		<b>Vocalist: <?php echo $row['vocalist'] ?></b><br>
		<b>Type: <?php echo $row['name'] ?></b><br>
		<b>Price: $<?php echo $row['price'] ?></b><br>
		<b>Summary: <?php echo $row['summary'] ?></b><br>
		<a class="btn btn-danger" href="album-del.php?id=<?php echo $row['id'] ?>" class="del">delete</a>
		<a class="btn btn-success" href="album-edit.php?id=<?php echo $row['id'] ?>">edit</a>
		<a class="btn btn-primary" href="album-new.php" class="new">New Album</a><br>
		<div>&nbsp;</div>
		<?php endwhile; ?>
	</div>
		<div class="col-md-6"></div>
	</div>
	</div>
</body>
</html>