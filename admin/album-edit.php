<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>Edit Album</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
  	<script src="jquery/jquery.min.js"></script>
 	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
</head>
<body>
	<div class="container">
		<?php
		include("confs/auth.php");
		include("confs/config.php");
		$id = $_GET['id'];
		$result = mysql_query("SELECT * FROM albums WHERE id = $id");
		$row = mysql_fetch_assoc($result);
		?>
  	  <h2>Edit Album</h2>
  	  <br>
  		<a class="btn btn-primary" href="album-list.php">Manage Albums</a>
		<a class="btn btn-primary" href="cat-list.php">Manage Categories</a>
		<a class="btn btn-primary" href="orders.php">Manage Orders</a>
		<a class="btn btn-danger" href="logout.php">Logout</a>
		<div>&nbsp;</div>
	  <form action="album-update.php" method="post" enctype="multipart/form-data">
	  	<input type="hidden" name="id" value="<?php echo $row['id'] ?>">
	    <div class="form-group">
	    	<label for="title">Album Title</label>
			<input type="text" class="form-control" name="title" id="title" value="<?php echo $row['title'] ?>">
	    </div>
	    <div class="form-group">
	    	<label for="vocalist">Vacalist</label>
			<input type="text" class="form-control" name="vocalist" id="vocalist" value="<?php echo $row['vocalist'] ?>">
	    </div>
	    <div class="form-group">
	    	<label for="summary">Summary</label>
	    	<textarea name="summary" class="form-control" id="summary"><?php echo $row['summary'] ?></textarea>
	    </div>
	    <div class="form-group">
	    	<label for="price">Price</label>
	    	<input type="text" class="form-control" name="price" id="price" value="<?php echo $row['price'] ?>">
	    </div>
	    <div class="form-group">
	    	<label for="categories">Category</label>
			<select class="form-control" name="category_id" id="categories">
			<option value="0">-- Choose --</option>
				<?php
				$categories = mysql_query("SELECT id, name FROM categories");
				while($cat = mysql_fetch_assoc($categories)):
				?>
				<option value="<?php echo $cat['id'] ?>"
				<?php if($cat['id'] == $row['category_id']) echo "selected" ?> >
				<?php echo $cat['name'] ?>
				</option>
				<?php endwhile; ?>
			</select>
			<div>&nbsp;</div>
			<img src="covers/<?php echo $row['cover'] ?>" class="img-rounded">
	    </div>
		<div class="form-group">
	    	<label for="cover">Change Cover</label>	
			<input type="file" name="cover" id="cover">
	    </div>
	    <button type="submit" class="btn btn-primary">Update Album</button>
	    <a class="btn btn-info" href="album-list.php" class="back">Back</a>	
	    <div>&nbsp;</div>
	  </form>
	</div>
</body>
</html>