<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>Category List</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
  	<script src="jquery/jquery.min.js"></script>
 	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
</head>
<body>
	<div class="container">
		<h2>Category List</h2>
		<br>
  		<a class="btn btn-primary" href="album-list.php">Manage Albums</a>
		<a class="btn btn-primary" href="cat-list.php">Manage Categories</a>
		<a class="btn btn-primary" href="orders.php">Manage Orders</a>
		<a class="btn btn-danger" href="logout.php">Logout</a>
		<div>&nbsp;</div>
		<?php
		include("confs/auth.php");
		include("confs/config.php");
		$result = mysql_query("SELECT * FROM categories");
		?>
		<?php while($row = mysql_fetch_assoc($result)): ?>
		<div title="<?php echo $row['remark'] ?>">
		<a class="btn btn-danger" href="cat-del.php?id=<?php echo $row['id'] ?>" class="del">delete</a>
		<a class="btn btn-success" href="cat-edit.php?id=<?php echo $row['id'] ?>">edit</a>
		<?php echo $row['name'] ?>
		<div>&nbsp;</div>
		</div>
		<?php endwhile; ?>
		<a class="btn btn-primary" href="cat-new.php" class="new">New Category</a>
	</div>
</body>
</html>