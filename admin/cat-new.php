<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>New Category</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
  	<script src="jquery/jquery.min.js"></script>
 	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
</head>
<body>
	<div class="container">
  	<h2>New Category</h2>
  	<br>
  		<a class="btn btn-primary" href="album-list.php">Manage Albums</a>
		<a class="btn btn-primary" href="cat-list.php">Manage Categories</a>
		<a class="btn btn-primary" href="orders.php">Manage Orders</a>
		<a class="btn btn-danger" href="logout.php">Logout</a>
		<div>&nbsp;</div>	
	  <form action="cat-add.php" method="post">
	    <div class="form-group">
	      <label for="name">Category Name:</label>
	      <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required>
	    </div>
	    <div class="form-group">
	      <label for="remark">Remark:</label>
	      <textarea class="form-control" id="remark" name="remark" placeholder="Enter remark" required></textarea>
	    </div>
	    <button type="submit" class="btn btn-primary">Add Category</button>
	  </form>
	</div>
</body>
</html>