<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>New Album</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
  	<script src="jquery/jquery.min.js"></script>
 	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
</head>
<body>
	<div class="container">
  	<h2>New Album</h2>
  	<br>
  		<a class="btn btn-primary" href="album-list.php">Manage Albums</a>
		<a class="btn btn-primary" href="cat-list.php">Manage Categories</a>
		<a class="btn btn-primary" href="orders.php">Manage Orders</a>
		<a class="btn btn-danger" href="logout.php">Logout</a>
		<div>&nbsp;</div>
	  <form action="album-add.php" method="post" enctype="multipart/form-data">
	    <div class="form-group">
	    	<label for="title">Album Title</label>
			<input type="text" class="form-control" name="title" id="title" placeholder="Enter album title">
	    </div>
	    <div class="form-group">
	    	<label for="vocalist">Vacalist</label>
			<input type="text" class="form-control" name="vocalist" id="vocalist" placeholder="Enter Vocalist">
	    </div>
	    <div class="form-group">
	    	<label for="summary">Summary</label>
	    	<textarea name="summary" class="form-control" id="summary" placeholder="Enter summary"></textarea>
	    </div>
	    <div class="form-group">
	    	<label for="price">Price</label>
	    	<input type="text" class="form-control" name="price" id="price" placeholder="Enter price">
	    </div>
	    <div class="form-group">
	    	<label for="categories">Category</label>
			<select class="form-control" name="category_id" id="categories">
			<option value="0">-- Choose --</option>
				<?php
				include("confs/auth.php");
				include("confs/config.php");
				$result = mysql_query("SELECT id, name FROM categories");
				while($row = mysql_fetch_assoc($result)):
				?>
			<option value="<?php echo $row['id'] ?>">
			<?php echo $row['name'] ?>
			</option>
			<?php endwhile; ?>
			</select>
	    </div>
		<div class="form-group">
	    	<label for="cover">Cover</label>	
			<input type="file" name="cover" id="cover">
	    </div>
	    <button type="submit" class="btn btn-primary">Add Album</button>
	    <a class="btn btn-info" href="album-list.php" class="back">Back</a>
	  </form>
	</div>
</body>
</html>