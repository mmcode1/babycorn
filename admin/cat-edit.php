<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>Edit Category</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
  	<script src="jquery/jquery.min.js"></script>
 	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
</head>
<body>
	<div class="container">
		<h2>Edit Category</h2>
		<br>
  		<a class="btn btn-primary" href="album-list.php">Manage Albums</a>
		<a class="btn btn-primary" href="cat-list.php">Manage Categories</a>
		<a class="btn btn-primary" href="orders.php">Manage Orders</a>
		<a class="btn btn-danger" href="logout.php">Logout</a>
		<div>&nbsp;</div>
		<?php
			include("confs/auth.php");
			include("confs/config.php");
			$id = $_GET['id'];
			$result = mysql_query("SELECT * FROM categories WHERE id = $id");
			$row = mysql_fetch_assoc($result);
		?>
		<form action="cat-update.php" method="post">
			<input type="hidden" name="id" value="<?php echo $row['id'] ?>">
			<div class="form-group">
				<label for="name">Category Name</label>
				<input type="text" class="form-control" name="name" id="name" placeholder="Enter name" required
				value="<?php echo $row['name'] ?>">
			</div>
			<div class="form-group">
				<label for="remark">Remark</label>
				<textarea name="remark" class="form-control"
				id="remark" placeholder="Enter remark" required><?php echo $row['remark'] ?></textarea>
			</div>
			<input type="submit" class="btn btn-primary" value="Update Category">
		</form>
	</div>
</body>
</html>