<?php
  session_start();
  if(!isset($_SESSION['cart'])) {
    header("location: index.php");
    exit();
  }

  include("admin/confs/config.php");
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <title>View Cart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="admin/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="admin/jquery/jquery.min.js"></script>
  <script src="admin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
  <h2>View Cart</h2>
      <a class="btn btn-info" href="index.php">&laquo; Continue Shopping</a>
      <a class="btn btn-info" href="clear-cart.php">&times; Clear Cart</a>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-8">&nbsp;</div>
        <div class="col-xs-6 col-md-4"></div>
      </div>
    <table class="table">
      <tr>
        <th>Album Title</th>
        <th>Quantity</th>
        <th>Unit Price</th>
        <th>Price</th>
      </tr>
      <?php
        $total = 0;
        foreach($_SESSION['cart'] as $id => $qty):
          $result = mysql_query("SELECT title, price FROM albums WHERE id=$id");
          $row = mysql_fetch_assoc($result);
          $total += $row['price'] * $qty;
      ?>
      <tr>
        <td><?php echo $row['title'] ?></td>
        <td><?php echo $qty ?></td>
        <td>$<?php echo $row['price'] ?></td>
        <td>$<?php echo $row['price'] * $qty ?></td>
      </tr>
      <?php endforeach; ?>
      <tr>
        <td colspan="3" align="right"><b>Total:</b></td>
        <td>$<?php echo $total; ?></td>
      </tr>
    </table>

        <h2>Order Now</h2>
        <form action="submit-order.php" method="post">
          <label for="name">Your Name</label>
          <input type="text" class="form-control" name="name" id="name" required>

          <label for="email">Email</label>
          <input type="text" class="form-control" name="email" id="email" required>

          <label for="phone">Phone</label>
          <input type="text" class="form-control" name="phone" id="phone" required>

          <label for="address">Address</label>
          <textarea name="address" class="form-control" id="address" required></textarea>

          <br><br>
          <input type="submit" class="btn btn-primary" value="Submit Order">&nbsp;
          <a href="index.php" class="btn btn-info">Continue Shopping</a>
        </form>
      </div>
    </div>
    </form>
  </div>
</body>
</html>
