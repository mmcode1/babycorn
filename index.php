<?php
	session_start();
	include("admin/confs/config.php");
	$cart = 0;
	if(isset($_SESSION['cart'])) {
	foreach($_SESSION['cart'] as $qty) {
	$cart += $qty;
	}
}
if(isset($_GET['cat'])) {
	$cat_id = $_GET['cat'];
	$albums = mysql_query("SELECT * FROM albums WHERE category_id = $cat_id");
	} else {
	$albums = mysql_query("SELECT * FROM albums");
	}
	$cats = mysql_query("SELECT * FROM categories");
?>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>Music Store</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="admin/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  	<script src="admin/jquery/jquery.min.js"></script>
 	<script src="admin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-3"><h2>Music Store</h2></div>
  			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
  			<div class="col-md-3">
  				<a href="view-cart.php">
					[<?php echo $cart ?>] albums in your cart
				</a>
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row">
			<div class="col-md-3"><a href="index.php">All Categories</a></div>
			<div class="col-md-3"></div>
  			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>	
		<div class="row">
			<div class="col-md-3">
				<?php while($row = mysql_fetch_assoc($cats)): ?>
				<a href="index.php?cat=<?php echo $row['id'] ?>">
				<?php echo $row['name'] ?>
				</a>	
			</div>
			<div class="col-md-3"></div>
  			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>
		<?php endwhile; ?>
		<?php while($row = mysql_fetch_assoc($albums)): ?>
		<div class="row">
			<div class="col-md-8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<img src="admin/covers/<?php echo $row['cover'] ?>" height="150">
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="album-detail.php?id=<?php echo $row['id'] ?>">
				<?php echo $row['title'] ?>
				</a>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>$<?php echo $row['price'] ?></i></div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="add-to-cart.php?id=<?php echo $row['id'] ?>">Add to Cart</a></div>
			<div class="col-md-4"></div>
		<?php endwhile; ?>
	</div>
</body>
</html>